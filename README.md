# Movies API
## Decathlon interview task

Application uses Django 3+ and Django Rest Framework.

Deployed on Heroku:
https://decathlon-movies-api.herokuapp.com/api/

All API endpoints are also documented on Postman:

https://app.getpostman.com/join-team?invite_code=e7f89b9e9c0d4bb70683543751bc0df7&ws=99e8d3e7-2cd0-47cd-8327-ea1e99aad426


*Make sure to switch to Prod environment in Postman to be able to start making requests!*


### Available endpoints
- POST /movies
- GET /movies
- GET /movies?movie={movieUUID}
- GET /movies?genre={genreName}
- PATCH /movies/{movieId}
- PUT /movies/{movieUUID}
- DELETE /movies/{movieUUID}
- POST /comments
- GET /comments
- GET /top?start=dd-mm-yyyy&end=dd-mm-yyyy


### Installation
To deploy on Heroku, simply login on your heroku account and push to proper repo branch.