from rest_framework import serializers
from core.models import Rating, Movie, Comment, Genre


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        ordering = ['created']
        read_only_fields = ('uuid', 'created', 'source', 'value', )
        fields = ('uuid', 'created', 'source', 'value', )


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        ordering = ['created']
        read_only_fields = ('title', )
        fields = ('uuid', 'title', )


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        ordering = ['created']
        read_only_fields = ('uuid', 'created',)
        fields = ('uuid', 'created', 'uuid', 'content', 'movie')


class MovieSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)
    ratings = RatingSerializer(many=True, read_only=True)
    genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        ordering = ['created']
        read_only_fields = ('uuid', 'created', )
        fields = ('uuid', 'created', 'title', 'comments', 'ratings', 'genres', 'year', 'runtime', 'rated', 'production',
                  'box_office', 'director', 'language', 'country', 'awards', 'poster', 'writer', 'plot')

