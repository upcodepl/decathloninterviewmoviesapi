from django.conf.urls import url, include
from django.urls import path
from api import views
from rest_framework import routers

app_name = "api"

router = routers.SimpleRouter()

router.register(r"movies", views.MovieViewset, basename="movies")
router.register(r"comments", views.CommentViewset, basename="comments")

urlpatterns = [
    path("", include(router.urls)),
    path("top", views.TopView.as_view(), name="top"),
]
