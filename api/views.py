from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Count
from core.models import Movie, Genre, Rating, Comment
from core.vendors.omdbapi import OmdbApiWrapper, MovieDoesNotExistException
from .serializers import MovieSerializer, CommentSerializer
import datetime


class TopView(APIView):
    def get(self, request, *args, **kwargs):
        # get 5 top movies (by comment)
        try:
            start = datetime.datetime.strptime(self.request.query_params.get('start', None), "%d-%m-%Y")
            end = datetime.datetime.strptime(self.request.query_params.get('end', None), "%d-%m-%Y")
        except Exception as e:
            return Response(data={
                'error': 'Please specify start and end query params in mm-dd-yyyy format'
            }, status=status.HTTP_400_BAD_REQUEST)

        if end < start:
            return Response(data={
                'error': 'End date cannot be lower than start date'
            }, status=status.HTTP_400_BAD_REQUEST)
        # Query all commends UUIDS made within given time range
        range_uuids = Comment.objects.filter(created__gte=start, created__lte=end).select_related('movie').values_list('movie__uuid', flat=True)


        qs = Movie.objects.filter(uuid__in=range_uuids).annotate(num_comments=Count('comments')).order_by('-num_comments')
        filered_nums = {}
        movies_uuids = []
        top_movies = []

        for index, movie in enumerate(qs):
            if len(filered_nums) < 5:
                if movie.num_comments not in filered_nums:
                    filered_nums[movie.num_comments] = []
                filered_nums[movie.num_comments].append(movie)
                movies_uuids.append(movie.uuid)
                top_movies.append({
                    'movie_id': movie.uuid,
                    'rank': len(filered_nums),
                    'total_comments': movie.num_comments
                })

        return Response(data=top_movies)


class CommentViewset(viewsets.ModelViewSet):
    '''
    Comment Resource
    '''

    def get_serializer_class(self):
        return CommentSerializer

    def get_serializer_context(self):
        return {'request': self.request}

    def get_queryset(self):
        qs = Comment.objects.select_related('movie')
        movie = self.request.query_params.get('movie', None)
        if movie:
            qs = qs.filter(movie=movie)
        return qs


class MovieViewset(viewsets.ModelViewSet):
    '''
    Movie Resource
    '''

    def get_serializer_class(self):
        return MovieSerializer

    def get_serializer_context(self):
        return {'request': self.request}

    def get_queryset(self):
        qs = Movie.objects.prefetch_related('genres', 'ratings', 'comments')
        genre = self.request.query_params.get('genre', None)
        if genre:
            qs = qs.filter(genres__title__icontains=genre)
        return qs

    def create(self, request, *args, **kwargs):
        '''
        We overwrite create method as we don't want to create data based on POST data - but on response from OMDBAPI,
        which has to validated first
        '''

        # First, we check if we have a movie already in our DB
        title = request.data.get('title')
        try:
            # we have a movie, return data with proper status code
            movie = Movie.objects.get(title=request.data.get('title'))
            serializer = MovieSerializer(movie)
            return Response(serializer.data)
        except Movie.DoesNotExist:
            # movie not in our database, lets try to get data from external API
            try:
                raw_movie_data = OmdbApiWrapper.search_movie(title)
            except MovieDoesNotExistException:
                # movie does not exist, raise Http 410 (invalid request)
                return Response(data={
                    'message': 'Movie does not exist'
                }, status=status.HTTP_400_BAD_REQUEST)
            else:
                # we got movie data
                # first, for our comfort - map OMDB API response to our format
                movie_data = OmdbApiWrapper.map_to_internal_format(raw_movie_data)
                serializer = self.get_serializer(data=movie_data)
                serializer.is_valid(raise_exception=True)
                instance = self.perform_create(serializer)

                for genre in movie_data['genres']:
                    genre_obj, created = Genre.objects.get_or_create(title=genre)
                    genre_obj.movies.add(instance)

                for rating in movie_data['ratings']:
                    rating_obj = Rating.objects.create(movie=instance, source=rating['Source'], value=rating['Value'])

                headers = self.get_success_headers(serializer.data)
                return Response(MovieSerializer(Movie.objects.get(uuid=instance.uuid)).data, status=status.HTTP_201_CREATED,
                                headers=headers)

    def perform_create(self, serializer):
        return serializer.save()
