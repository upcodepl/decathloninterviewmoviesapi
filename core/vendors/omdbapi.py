import requests
from django.conf import settings
import datetime


class MovieDoesNotExistException(Exception):
    pass


class OmdbApiWrapper(object):
    @staticmethod
    def search_movie(title):
        params = {
            'apikey': settings.OMDBAPI_KEY,
            't': title
        }
        data = requests.get('http://www.omdbapi.com/', params=params).json()
        if data.get('Response', None) != 'True':
            raise MovieDoesNotExistException
        else:
            return data

    @staticmethod
    def map_to_internal_format(raw_data):
        return {
            'title': raw_data.get('Title'),
            'year': int(raw_data.get('Year', None)),
            'released': datetime.datetime.strptime(raw_data.get('Released', None), "%d %b %Y"),
            'runtime': raw_data.get('Runtime', None).replace(' min', ''),
            'genres': raw_data.get('Genre', None).split(','),
            'writer': raw_data.get('Writer', None),
            'director': raw_data.get('Director', None),
            'awards': raw_data.get('Awards', None),
            'plot': raw_data.get('Plot', None),
            'rated': raw_data.get('Rated', None),
            'ratings': raw_data.get('Ratings', None),
            'language': raw_data.get('Language', None),
            'country': raw_data.get('Country', None),
            'actors': raw_data.get('Actors', None),
            'box_office': raw_data.get('BoxOffice', None),
            'production': raw_data.get('Production', None),
            'poster': raw_data.get('Poster', None),

        }
