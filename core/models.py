import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    """
    Base model for Movies API for Decathlon
    """

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    created = models.DateTimeField(
        auto_now_add=True, null=True, editable=False, verbose_name=_("Created")
    )
    modified = models.DateTimeField(
        auto_now=True, null=True, editable=False, verbose_name=_("Modified")
    )

    @property
    def short_uuid(self):
        return str(self.uuid)[:8]

    class Meta:
        abstract = True


class Genre(BaseModel):
    title = models.CharField(_('Genre'), max_length=512)


class Movie(BaseModel):
    title = models.CharField(_('Title'), max_length=512)
    year = models.PositiveIntegerField(null=True, blank=True)
    released = models.DateField(_('Released'), null=True, blank=True)
    runtime = models.CharField(_('Runtime'), null=True, blank=True, max_length=32)
    rated = models.CharField(_('Rated'), max_length=128, null=True, blank=True)
    production = models.CharField(_('Production'), max_length=512, null=True, blank=True)
    box_office = models.CharField(_('Box office'), max_length=512, null=True, blank=True)
    director = models.CharField(_('Director'), max_length=1024, null=True, blank=True)
    language = models.CharField(_('Language'), max_length=1024, null=True, blank=True)
    country = models.CharField(_('Country'), max_length=1024, null=True, blank=True)
    awards = models.CharField(_('Awards'), max_length=1024, null=True, blank=True)
    poster = models.URLField(_('Poster'), null=True, blank=True)
    writer = models.CharField(_('Writer'), max_length=1024, null=True, blank=True)
    actors = models.TextField(_('Actors'), null=True, blank=True)
    plot = models.TextField(_('Plot'), null=True, blank=True)

    genres = models.ManyToManyField(Genre, related_name='movies')


class Rating(BaseModel):
    movie = models.ForeignKey(Movie, related_name='ratings', on_delete=models.CASCADE)
    source = models.CharField(_('Source'), max_length=512)
    value = models.CharField(_('Value'), max_length=512)


class Comment(BaseModel):
    movie = models.ForeignKey(Movie, related_name='comments', on_delete=models.CASCADE)
    content = models.TextField(null=False, blank=False)
